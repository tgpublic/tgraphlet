#include "TemporalGraphletKernel.h"
#include "temporalmotifs/temporalmotifs.h"
#include <Eigen/Sparse>
#include <iterator>
#include "GraphKernelCommonFunctions.h"
#include "util/Timer.h"

using namespace std;

vector<int> countEdgePatterns(TemporalGraphStream &tgs, uint delta) {
    vector<int> patterns(4,0);
    for (auto &e : tgs.edges) {
        if (tgs.getNodeLabelAtTime(e.u_id, e.t) == 0 && tgs.getNodeLabelAtTime(e.v_id, e.t+delta) == 0) {
            patterns[0]++;
        }
        if (tgs.getNodeLabelAtTime(e.u_id, e.t) == 0 && tgs.getNodeLabelAtTime(e.v_id, e.t+delta) == 1) {
            patterns[1]++;
        }
        if (tgs.getNodeLabelAtTime(e.u_id, e.t) == 1 && tgs.getNodeLabelAtTime(e.v_id, e.t+delta) == 0) {
            patterns[2]++;
        }
        if (tgs.getNodeLabelAtTime(e.u_id, e.t) == 1 && tgs.getNodeLabelAtTime(e.v_id, e.t+delta) == 1) {
            patterns[3]++;
        }
    }
    return patterns;
}

void computeTemporalGraphletKernelGramMatrix(TemporalGraphStreams &data, std::string const &datasetname, bool stars, bool trias) {
    using SpMatrix = Eigen::SparseMatrix<double>;
    using GramMatrix = SpMatrix;
    using S = Eigen::Triplet<unsigned int>;
    Timer timer;

    bool twoNode = false; //always false

    long num_graphs = (long)data.size();
    vector<S> nonzero_components;

    // compute motifs
    vector<uint> deltas = {10, 100, 1000, 10000};
    for (int d = 0; d < deltas.size(); ++d) {

        timer.startTimer();

        auto delta = deltas[d];

        int num_graph = 0;
        int index = 0;
        for (auto &tgs: data) {
            TempMotifCounter tmc(tgs);
            vector<uint> counts;
            tmc.Count3TEdge23Node(delta, counts, twoNode, stars, trias);

            index = 0;
            for (auto count : counts) {
                nonzero_components.emplace_back(num_graph, index++, count);
            }

            if (stars) {
                auto ep = countEdgePatterns(tgs, delta);
                for (auto c: ep) {
                    nonzero_components.emplace_back(num_graph, index++, c);
                }
            }

            num_graph++;
        }

        // Compute Gram matrix.
        GramMatrix feature_vectors(num_graphs, index);

        feature_vectors.setFromTriplets(nonzero_components.begin(), nonzero_components.end());
        GramMatrix gram_matrix(num_graphs, num_graphs);
        gram_matrix = feature_vectors * feature_vectors.transpose();

        Eigen::MatrixXd dense_gram_matrix(gram_matrix);

        Eigen::MatrixXd nm;
        CommonFunctions::normalizeGramMatrix(dense_gram_matrix, nm);

        string suff = to_string(stars)+ to_string(trias);
        auto full_name = datasetname + "__tkg" + suff + "_" + to_string(d) + ".gram";
        CommonFunctions::writeGramMatrixToFile(nm, data, full_name);
        timer.stopTimer(full_name + ".time");

    }

}
