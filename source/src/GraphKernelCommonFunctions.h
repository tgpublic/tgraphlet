#ifndef TGKERNEL_ABSTRACTKERNEL_H
#define TGKERNEL_ABSTRACTKERNEL_H

#include "util/TemporalGraphs.h"
#include <Eigen/Core>
#include <fstream>
#include <cmath>
#include <chrono>
#include <iostream>

namespace CommonFunctions {

    template<typename T, typename U>
    void normalizeGramMatrix(T &m, U &nm) {
        unsigned long size = m.rows();
        nm.resize(size, size);
        for (unsigned int i = 0; i < size; ++i) {
            for (unsigned int j = 0; j < size; ++j) {
                double x = (sqrt(m(i, i)) * sqrt(m(j, j)));
                if (x != 0)
                    nm(i, j) = m(i, j) / x;
                else
                    nm(i, j) = 0;
                if (std::isnan(nm(i, j))) nm(i, j) = 0;
            }
        }
    }

    // write libsvm style gram matrix to file
    template<typename T>
    void writeGramMatrixToFile(T m, TemporalGraphStreams &data, std::string const &filename) {
        std::ofstream gramFile;
        gramFile.open(filename);
        for (size_t i = 0; i < data.size(); ++i) {
            gramFile << data.at(i).label << " 0:" << (i + 1);
            for (size_t c = 0; c < data.size(); ++c) {
                gramFile << " " << (c + 1) << ":" << m(i, c);
            }
            gramFile << std::endl;
        }
        gramFile.close();
    }

}
#endif //TGKERNEL_ABSTRACTKERNEL_H
