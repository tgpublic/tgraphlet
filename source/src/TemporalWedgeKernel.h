#ifndef TGRAPHLET_TEMPORALWEDGEKERNEL_H
#define TGRAPHLET_TEMPORALWEDGEKERNEL_H


#include "util/TemporalGraphs.h"

void computeTemporalGraphletKernelApproxGramMatrix(TemporalGraphStreams &data, std::string const &datasetname, uint num_samples, bool use_delta);

void computeTemporalGraphletKernelWedgesGramMatrix(TemporalGraphStreams &data, std::string const &datasetname);


#endif //TGRAPHLET_TEMPORALWEDGEKERNEL_H
