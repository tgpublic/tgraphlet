#ifndef TGKERNEL_TEMPORALGRAPHLETKERNEL_H
#define TGKERNEL_TEMPORALGRAPHLETKERNEL_H

#include "util/TemporalGraphs.h"

void computeTemporalGraphletKernelGramMatrix(TemporalGraphStreams &data, std::string const &datasetname, bool stars, bool trias);

#endif //TGKERNEL_TEMPORALGRAPHLETKERNEL_H
