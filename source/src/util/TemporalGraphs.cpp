#include "TemporalGraphs.h"
#include <string>

using namespace std;

void TemporalGraphStream::make_simple() {
    TemporalEdges tes;
    unsigned int num_multiedges = 0;
    for (size_t i = 0; i < edges.size(); ++i){
        TemporalEdge &e = edges.at(i);
        bool found = false;
        for (size_t j = i + 1; j < edges.size(); ++j){
            TemporalEdge &f = edges.at(j);
            if (e.u_id == f.u_id && e.v_id == f.v_id && e.t == f.t) {
                found = true;
                ++num_multiedges;
                break;
            }
        }
        if (!found) {
            tes.push_back(e);
        }
    }
    edges.clear();
    edges.insert(edges.begin(), tes.begin(), tes.end());
}


TemporalGraph TemporalGraphStream::toTemporalGraph() {
    TemporalGraph g;

    for (unsigned int i = 0; i < get_num_nodes(); ++i) {
        TGNode node;
        node.id = i;
        if (timed_node_labels.size() > i)
            node.timed_labels = timed_node_labels.at(i);
        if (!static_node_labels.empty()) {
            node.static_label = static_node_labels.at(i);
        }
        g.nodes.push_back(node);
        g.num_nodes++;
    }

    for (TemporalEdge e : edges) {
        g.nodes.at(e.u_id).adjlist.push_back(e);
        g.num_edges++;
    }

    g.max_time = get_max_time();
    g.label = label;

    return g;
}


void TemporalGraphStream::sort_edges() {
    std::sort(edges.begin(), edges.end(), [](const TemporalEdge & a, const TemporalEdge & b) -> bool { return a.t < b.t; });
}


Labels TemporalGraphStream::getFlattenedNodeLabels() {
    Labels result;
    for (auto &l : timed_node_labels) {
        Label fl = 0;
        for (auto &time_label : l){
            fl = max(fl, time_label.second);
        }
        result.push_back(fl);
    }
    return result;
}

Label TemporalGraphStream::getNodeLabelAtTime(NodeId nid, Time time) {
    if (timed_node_labels.at(nid).empty()) return 0;
    Label result = timed_node_labels.at(nid).at(0).second;
    for (auto &l : timed_node_labels.at(nid)) {
        if (l.first > time) break;
        result = l.second;
    }
    return result;
}


std::ostream& operator<<(std::ostream& os, const TemporalGraphStream& tgs) {
    std::string s1 = "num_nodes\t" + std::to_string(tgs.get_num_nodes()) + "\n";
    std::string s2 = "num_edges\t" + std::to_string(tgs.edges.size()) + "\n";
    std::string s3 = "max_time\t" + std::to_string(tgs.get_max_time()) + "\n";

    return os << s1 << s2 << s3;
}

std::ostream& operator<<(std::ostream& os, const TemporalEdge &tgs) {
    std::string s1 = std::to_string(tgs.u_id) + " " + std::to_string(tgs.v_id) + " " + std::to_string(tgs.t) + " " + "\n";

    return os << s1;
}

